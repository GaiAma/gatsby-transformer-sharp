"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.gatsbyImageSharpSizesPreferWebpNoBase64 = exports.gatsbyImageSharpSizesNoBase64 = exports.gatsbyImageSharpSizesPreferWebpTracedSVG = exports.gatsbyImageSharpSizesPreferWebp = exports.gatsbyImageSharpSizesTracedSVG = exports.gatsbyImageSharpSizes = exports.gatsbyImageSharpResolutionsPreferWebpNoBase64 = exports.gatsbyImageSharpResolutionsNoBase64 = exports.gatsbyImageSharpResolutionsPreferWebpTracedSVG = exports.gatsbyImageSharpResolutionsPreferWebp = exports.gatsbyImageSharpResolutionsTracedSVG = exports.gatsbyImageSharpResolutions = undefined;

var _taggedTemplateLiteral2 = require("babel-runtime/helpers/taggedTemplateLiteral");

var _taggedTemplateLiteral3 = _interopRequireDefault(_taggedTemplateLiteral2);

var _templateObject = (0, _taggedTemplateLiteral3.default)(["\n  fragment GatsbyImageSharpResolutions on ImageSharpResolutions {\n    base64\n    width\n    height\n    src\n    srcSet\n  }\n"], ["\n  fragment GatsbyImageSharpResolutions on ImageSharpResolutions {\n    base64\n    width\n    height\n    src\n    srcSet\n  }\n"]),
    _templateObject2 = (0, _taggedTemplateLiteral3.default)(["\n  fragment GatsbyImageSharpResolutions_tracedSVG on ImageSharpResolutions {\n    tracedSVG\n    width\n    height\n    src\n    srcSet\n  }\n"], ["\n  fragment GatsbyImageSharpResolutions_tracedSVG on ImageSharpResolutions {\n    tracedSVG\n    width\n    height\n    src\n    srcSet\n  }\n"]),
    _templateObject3 = (0, _taggedTemplateLiteral3.default)(["\n  fragment GatsbyImageSharpResolutions_withWebp on ImageSharpResolutions {\n    base64\n    width\n    height\n    src\n    srcSet\n    srcWebp\n    srcSetWebp\n  }\n"], ["\n  fragment GatsbyImageSharpResolutions_withWebp on ImageSharpResolutions {\n    base64\n    width\n    height\n    src\n    srcSet\n    srcWebp\n    srcSetWebp\n  }\n"]),
    _templateObject4 = (0, _taggedTemplateLiteral3.default)(["\n  fragment GatsbyImageSharpResolutions_withWebp_tracedSVG on ImageSharpResolutions {\n    tracedSVG\n    width\n    height\n    src\n    srcSet\n    srcWebp\n    srcSetWebp\n  }\n"], ["\n  fragment GatsbyImageSharpResolutions_withWebp_tracedSVG on ImageSharpResolutions {\n    tracedSVG\n    width\n    height\n    src\n    srcSet\n    srcWebp\n    srcSetWebp\n  }\n"]),
    _templateObject5 = (0, _taggedTemplateLiteral3.default)(["\n  fragment GatsbyImageSharpResolutions_noBase64 on ImageSharpResolutions {\n    width\n    height\n    src\n    srcSet\n  }\n"], ["\n  fragment GatsbyImageSharpResolutions_noBase64 on ImageSharpResolutions {\n    width\n    height\n    src\n    srcSet\n  }\n"]),
    _templateObject6 = (0, _taggedTemplateLiteral3.default)(["\n  fragment GatsbyImageSharpResolutions_withWebp_noBase64 on ImageSharpResolutions {\n    width\n    height\n    src\n    srcSet\n    srcWebp\n    srcSetWebp\n  }\n"], ["\n  fragment GatsbyImageSharpResolutions_withWebp_noBase64 on ImageSharpResolutions {\n    width\n    height\n    src\n    srcSet\n    srcWebp\n    srcSetWebp\n  }\n"]),
    _templateObject7 = (0, _taggedTemplateLiteral3.default)(["\n  fragment GatsbyImageSharpSizes on ImageSharpSizes {\n    base64\n    aspectRatio\n    src\n    srcSet\n    sizes\n  }\n"], ["\n  fragment GatsbyImageSharpSizes on ImageSharpSizes {\n    base64\n    aspectRatio\n    src\n    srcSet\n    sizes\n  }\n"]),
    _templateObject8 = (0, _taggedTemplateLiteral3.default)(["\n  fragment GatsbyImageSharpSizes_tracedSVG on ImageSharpSizes {\n    tracedSVG\n    aspectRatio\n    src\n    srcSet\n    sizes\n  }\n"], ["\n  fragment GatsbyImageSharpSizes_tracedSVG on ImageSharpSizes {\n    tracedSVG\n    aspectRatio\n    src\n    srcSet\n    sizes\n  }\n"]),
    _templateObject9 = (0, _taggedTemplateLiteral3.default)(["\n  fragment GatsbyImageSharpSizes_withWebp on ImageSharpSizes {\n    base64\n    aspectRatio\n    src\n    srcSet\n    srcWebp\n    srcSetWebp\n    sizes\n  }\n"], ["\n  fragment GatsbyImageSharpSizes_withWebp on ImageSharpSizes {\n    base64\n    aspectRatio\n    src\n    srcSet\n    srcWebp\n    srcSetWebp\n    sizes\n  }\n"]),
    _templateObject10 = (0, _taggedTemplateLiteral3.default)(["\n  fragment GatsbyImageSharpSizes_withWebp_tracedSVG on ImageSharpSizes {\n    tracedSVG\n    aspectRatio\n    src\n    srcSet\n    srcWebp\n    srcSetWebp\n    sizes\n  }\n"], ["\n  fragment GatsbyImageSharpSizes_withWebp_tracedSVG on ImageSharpSizes {\n    tracedSVG\n    aspectRatio\n    src\n    srcSet\n    srcWebp\n    srcSetWebp\n    sizes\n  }\n"]),
    _templateObject11 = (0, _taggedTemplateLiteral3.default)(["\n  fragment GatsbyImageSharpSizes_noBase64 on ImageSharpSizes {\n    aspectRatio\n    src\n    srcSet\n    sizes\n  }\n"], ["\n  fragment GatsbyImageSharpSizes_noBase64 on ImageSharpSizes {\n    aspectRatio\n    src\n    srcSet\n    sizes\n  }\n"]),
    _templateObject12 = (0, _taggedTemplateLiteral3.default)(["\n  fragment GatsbyImageSharpSizes_withWebp_noBase64 on ImageSharpSizes {\n    aspectRatio\n    src\n    srcSet\n    srcWebp\n    srcSetWebp\n    sizes\n  }\n"], ["\n  fragment GatsbyImageSharpSizes_withWebp_noBase64 on ImageSharpSizes {\n    aspectRatio\n    src\n    srcSet\n    srcWebp\n    srcSetWebp\n    sizes\n  }\n"]);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/* eslint-disable */
var gatsbyImageSharpResolutions = exports.gatsbyImageSharpResolutions = graphql(_templateObject);

var gatsbyImageSharpResolutionsTracedSVG = exports.gatsbyImageSharpResolutionsTracedSVG = graphql(_templateObject2);

var gatsbyImageSharpResolutionsPreferWebp = exports.gatsbyImageSharpResolutionsPreferWebp = graphql(_templateObject3);

var gatsbyImageSharpResolutionsPreferWebpTracedSVG = exports.gatsbyImageSharpResolutionsPreferWebpTracedSVG = graphql(_templateObject4);

var gatsbyImageSharpResolutionsNoBase64 = exports.gatsbyImageSharpResolutionsNoBase64 = graphql(_templateObject5);

var gatsbyImageSharpResolutionsPreferWebpNoBase64 = exports.gatsbyImageSharpResolutionsPreferWebpNoBase64 = graphql(_templateObject6);

var gatsbyImageSharpSizes = exports.gatsbyImageSharpSizes = graphql(_templateObject7);

var gatsbyImageSharpSizesTracedSVG = exports.gatsbyImageSharpSizesTracedSVG = graphql(_templateObject8);

var gatsbyImageSharpSizesPreferWebp = exports.gatsbyImageSharpSizesPreferWebp = graphql(_templateObject9);

var gatsbyImageSharpSizesPreferWebpTracedSVG = exports.gatsbyImageSharpSizesPreferWebpTracedSVG = graphql(_templateObject10);

var gatsbyImageSharpSizesNoBase64 = exports.gatsbyImageSharpSizesNoBase64 = graphql(_templateObject11);

var gatsbyImageSharpSizesPreferWebpNoBase64 = exports.gatsbyImageSharpSizesPreferWebpNoBase64 = graphql(_templateObject12);